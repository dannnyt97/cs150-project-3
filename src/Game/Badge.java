/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game;

/**
 *
 * @author dtwns
 */
public class Badge {
    public String[] badgeNames;
    private String name;
    private int pointsPerCategory;
    private String stat;
    
    public Badge() {
        this.name = "";
        this.pointsPerCategory = 0;
        this.stat = "";
        
        
        badgeNames = new String[]{"Healer", "Explorer", "Socialite", "Contributor,"
                                + "Hoarder", "Fixer", "Joiner", "Leader", "Punisher", 
                                  "Obsessed"};
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPointsPerCategory() {
        return pointsPerCategory;
    }

    public void setPointsPerCategory(int pointsPerCategory) {
        this.pointsPerCategory = pointsPerCategory;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }
    
    
    
    
    
}
